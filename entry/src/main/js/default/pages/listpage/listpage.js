import router from '@system.router';

export default {
    data: {
        refreshhight: 0,
        listtitle: '',
        refreshtitle: '下拉刷新',//刷新文本提示
        loadhight: 0,
        startheight: 0,//开始位置
        touchheight: 0,// 移动高度
        playState: '',
        contentlist: [{
                          title: 'Rasmus Saari',
                          detail: 'Muhos, Finland',
                      }, {
                          title: 'Victoria Barnaby',
                          detail: 'Fountainbleu, Canada',
                      }, {
                          title: 'Anton Justi',
                          detail: 'Ulvila, Finland',
                      }, {
                          title: 'Gudula Bätz',
                          detail: 'Roth, Germany',
                      }, {
                          title: 'Lucas Brown',
                          detail: 'Vanier, Canada',
                      }, {
                          title: 'Dyonne De Roo',
                          detail: 'Hekelingen, Netherlands',
                      }, {
                          title: 'Rasmus Saari',
                          detail: 'Muhos, Finland',
                      }, {
                          title: 'Victoria Barnaby',
                          detail: 'Fountainbleu, Canada',
                      }, {
                          title: 'Anton Justi',
                          detail: 'Ulvila, Finland',
                      }, {
                          title: 'Gudula Bätz',
                          detail: 'Roth, Germany',
                      }],
    },
    onInit() {
        this.title = this.$t('strings.listtitle');//设置标题
    },
    // 手势滑动点击监听
    touchstartfunc(msg) {
        console.info(`on touch start, touchstart is: ${msg.touches[0].globalY}`);
        this.startheight = msg.touches[0].globalY;
    },
    // 手势滑动移动监听
    touchmovefunc(msg) {
        console.info(`on touch start, touchmove is: ${msg.touches[0].globalY}`);
        let mavegeight = msg.touches[0].globalY - this.startheight;
        this.touchheight = mavegeight;
        console.info('===== mavegeight = ' + mavegeight)
        if (mavegeight > 1 && mavegeight <= 250) {
            this.refreshtitle = "下拉刷新";
        } else {
            this.refreshtitle = "松手刷新";
        }
        if (mavegeight > 10 && mavegeight <= 400) {
            this.refreshhight = mavegeight;
        }
    },
    // 手势滑动抬起监听
    touchendfunc(msg) {
        // 刷新的时候 松手，操作网络接口 并且 隐藏刷新效果
        if (this.touchheight > 10) {
            if (this.touchheight < 250) {
                this.refreshhight = 0;
                return;
            }

            this.refreshhight = 100;
            console.info('this.touchendfunc')
            this.refreshtitle = "刷新中...";
            // 3秒后隐藏刷新
            setTimeout(() => {
                this.refreshhight = 0;
            }, 2000);
        }
    },
    // 列表滚动到底部监听
    scrollbottomfunc(msg) { // 滑动到底部 加载更多
        this.loadhight = 50;
        let button = this.$element('button');//获取属性id是button的信息
        var timeoutID = setTimeout(() => {
            this.loadhight = 0;
            button.scrollTo(10);
            clearTimeout(timeoutID);
        }, 2000);
    },
    scrolltopfunc(msg) {// 滑动到顶部
//        console.info(`on touch start, touchmove is: ${msg.touches[0].globalY}`);
        this.loadhight = 0;
    },
    scrollfunc(msg) {// 滑动
         console.info(`scrollfunc: ${msg.y}`);
    }
}
